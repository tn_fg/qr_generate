<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Image;
use QrCode;

class QrController extends Controller {

	public function qr_generate(Request $request) {
		// Se recolectan los datos
		$qr_txt = collect($request->input('qr_txt', []));
		// Decodifica datos de la imagen codificada
		$image = base64_decode($qr_txt['Foto']);
		// Guarda la imagen en la carpeta public
		// Storage::disk('public')->put('ejemplo.jpeg', $image);

		// Cambia el tamaño de la imagen a un tamaño fijo
		$img = Image::make($image)->resize(50, 50, function ($constraint) {
			$constraint->aspectRatio();
			$constraint->upsize();
		});
		// Guarda la nueva imagen editada
		$img_encode = $img->encode('jpg', 45);
		// Storage::disk('public')->put('ejemplo_risize.jpg', (string) $img->encode('jpg', 45));
		// $qr_txt['img'] = (string) Image::make(base_path('storage/app/public/ejemplo_risize.jpg'))->encode('data-url', 45);
		$qr_txt['Foto'] = (string) Image::make($img_encode)->encode('data-url', 45);
		// $qr_txt['img'] = base64_encode($img);
		// Aqui debe de cifrar
		$cifrar_code_status = null;
		$cifrar_return = null;
		// soap_cifrar('');
		// dd();
		// Genera el XML para el servicio de HSM
		$qr_hsm_cifrado = create_xml($qr_txt->toJson());
		// Envia los datos al programa codificador en formato json
		// $cmd = 'python3.6 ' . storage_path('scripts/cifrar.py') . " '" . $qr_txt->toJson() . "'";
		// // $cmd = 'python3.6 ' . storage_path('scripts/cifrar.py') . ' "texto a cifrar"';
		// // Comando para mandar datos al programa python
		// $qr_txt_cifrado = exec($cmd, $cifrar_return, $cifrar_code_status);
		// // dd($qr_txt_cifrado,$cifrar_code_status,$cifrar_return,$qr_txt->toJson());
		// // return response()->json([$cmd, $cifrar_return, $cifrar_code_status],200);
		// // Lo convierte en un Json
		// $qr_txt_cifrado = collect(json_decode($qr_txt_cifrado));
		// //Obtiene la longitud del texto
		$size = strlen($qr_hsm_cifrado);
		$txt1 = "1," . substr($qr_hsm_cifrado, 0, intval($size / 2));
		$txt2 = "2," . substr($qr_hsm_cifrado, intval($size / 2));

		// dd($qr_hsm_cifrado,$txt1,$txt2);
		// Guarda los QR generdos en la carpeta asignada
		$qr1_name  = "qr1".date('YmdHis');
		$qr2_name  = "qr2".date('YmdHis');
		$qr1 = QrCode::size(295)->format('png')->generate($txt1, storage_path('app/public/'.$qr1_name.'.png'));
		$qr2 = QrCode::size(295)->format('png')->generate($txt2, storage_path('app/public/'.$qr2_name.'.png'));
		// Obtenemos la ruta de la imagen
		$QR1_path = storage_path('app/public/'.$qr1_name.'.png');
		// Obtenemos la extension (formato) de la imagen
		$QR1_type = pathinfo($QR1_path, PATHINFO_EXTENSION);
		$QR1_data = file_get_contents($QR1_path);
		// Codificamos la imagen en base 64
		$QR1_base64 = 'data:image/' . $QR1_type . ';base64,' . base64_encode($QR1_data);

		$QR2_path = storage_path('app/public/'.$qr2_name.'.png');
		$QR2_type = pathinfo($QR2_path, PATHINFO_EXTENSION);
		$QR2_data = file_get_contents($QR2_path);
		$QR2_base64 = 'data:image/' . $QR2_type . ';base64,' . base64_encode($QR2_data);

		// Storage::disk('public')->delete($qr2_name.'.png');
		// Storage::disk('public')->delete($qr1_name.'.png');

		return response()
			->json(['QR1' => $QR1_base64, 'QR2' => $QR2_base64], 200);

		// return response()->json(['size' => $size, 'txt1' => $txt1, 'txt2' => $txt2, 'qr_txt' => $cifrar_return], 200);
	}

}
