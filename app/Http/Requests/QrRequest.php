<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class QrRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "Foto" => 'required|image',
            "NOMBRE" =>'required|string|max:200',
            "APELLIDOS" =>'nullable|string|max:200',
            "CURP" =>'nullable|string|max:200',
            "NACIONALIDAD" =>'nullable|string|max:200',
            "FECHA_NACIMIENTO" =>'nullable|string|max:200',
            "SEXO" =>'nullable|string|max:200',
            "DOCUMENTO" =>'nullable|string|max:200',
            "NUE" =>'nullable|string|max:200',
            "FECHA_EXPEDICION" =>'nullable|string|max:200',
            "OR_EXPEDICION" =>'nullable|string|max:200',
        ];
    }
}
