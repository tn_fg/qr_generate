<?php

function create_xml($qr)
{
	// Convertimos la matriz a una cadena con formato XML.
	$textoXML = '<?xml version="1.0" encoding="UTF-8"?>';
	$textoXML .= "\n";
	$textoXML .= '<soap:Envelope xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema">';
	$textoXML .= "\n";
	$textoXML .= "\t";
	$textoXML .= '<soap:Body>';
	$textoXML .= "\n";
	$textoXML .= "\t\t";
	$textoXML .= '<ProcessMessage xmlns="urn:sgdata">';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t";
	$textoXML .= '<request xmlns:q1="urn:sgdata" xsi:type="q1:EncryptQRRequest" xmlns="">';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t\t";
	$textoXML .= '<QR2EncryptList>';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t\t\t";
	$textoXML .= '<IdTran>0</IdTran>';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t\t\t";
	$textoXML .= ' <QR>'.base64_encode($qr).'</QR>';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t\t";
	$textoXML .= '</QR2EncryptList>';
	$textoXML .= "\n";
	$textoXML .= "\t\t\t";
	$textoXML .= '</request>';
	$textoXML .= "\n";
	$textoXML .= "\t\t";
	$textoXML .= '</ProcessMessage>';
	$textoXML .= "\n";
	$textoXML .= "\t";
	$textoXML .= '</soap:Body>';
	$textoXML .= "\n";
	$textoXML .= '</soap:Envelope>';
	
	// Nos aseguramos de que la cadena que contiene el XML esté en UTF-8
	$textoXML = mb_convert_encoding($textoXML, "UTF-8");
	return peticion_post("http://200.66.66.213:9000", $textoXML);
	$gestor = fopen( storage_path('app/public/obras.xml'), 'w');
	fwrite($gestor, $textoXML);
	fclose($gestor);
	return $textoXML;
}

function soap_cifrar($qr)
{
	$url = "http://200.66.66.213:9000";
	try {
		$client = new \SoapClient($url, [ "trace" => 1 ] );
		$result = $client->ProcessMessage( [ "QR" => base64_encode($qr->toJson()), "IdTran" => "1" ] );

		dd($result);
	} catch ( SoapFault $e ) {
		echo $e->getMessage();
	}
}
function peticion_post($url,$data)
{	 
	//inicializamos el objeto CUrl
	$ch = curl_init($url);
	 
	//Indicamos que nuestra petición sera Post
	curl_setopt($ch, CURLOPT_POST, 1);
	 
	 //para que la peticion no imprima el resultado como un echo comun, y podamos manipularlo
	 curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	 
	//Adjuntamos el json a nuestra petición
	curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
	 
	//Agregamos los encabezados del contenido
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml; charset=utf-8'));
	 
	 //ignorar el certificado, servidor de desarrollo
	          //utilicen estas dos lineas si su petición es tipo https y estan en servidor de desarrollo
	 //curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
	         //curl_setopt($process, CURLOPT_SSL_VERIFYHOST, FALSE);
	 
	//Ejecutamos la petición
	$result = curl_exec($ch);

	$cmd = 'python3.6 ' . storage_path('scripts/get_xml.py') . " '" . $result . "'";
	// $cmd = 'python3.6 ' . storage_path('scripts/cifrar.py') . ' "texto a cifrar"';
	// Comando para mandar datos al programa python
	$qr_txt_cifrado = exec($cmd, $cifrar_return, $cifrar_code_status);
	return $qr_txt_cifrado;
	dd($qr_txt_cifrado,$result);
}