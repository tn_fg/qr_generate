#!/usr/bin/python3.6

import json
from base64 import b64encode
from base64 import b64decode
from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
import binascii,os
import sys

from pkcs7 import PKCS7Encoder

encoder = PKCS7Encoder()

def pad(m):
    return m+chr(16-len(m)%16)*(16-len(m)%16)

def unpad(ct):
    return ct[:-ct[-1]]

#Cifrado
block_size = 20
key = 'Esta es la clave'
data = sys.argv[1]
# key = sys.argv[2]
iv = os.urandom(16)
pad_text = encoder.encode(data)
cipher = AES.new(key, AES.MODE_CFB,iv,segment_size=64)
# En MODO OFB requiere que el texto a cifrar sea multiplo de 16
# cipher = AES.new(key, AES.MODE_OFB,iv)
ct_bytes = cipher.encrypt(pad_text)
iv = b64encode(iv).decode('utf-8')
ct = b64encode(ct_bytes).decode('utf-8')
result = json.dumps({'iv':iv, 'ciphertext':ct})
print(result)


#Descifrado
#Suponemos que la clave se compartió de forma segura de antemano
# b64 = json.loads(result)
# iv2 = b64decode(b64['iv'])
# ct2 = b64decode(b64['ciphertext'])
# cipher = AES.new(key, AES.MODE_CFB, iv2,segment_size=64)
# pt = cipher.decrypt(ct2)
# print("The message was: ", pt)


