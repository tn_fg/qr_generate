@extends('layouts.main')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Generador de QR  <a href="/"> REGRESAR</a></div>

                <div class="card-body">
                    <img src="{{$QR1}}" class="rounded float-left" alt="...">
                    <img src="{{$QR2}}" class="rounded float-right" alt="...">


                </div>
            </div>
        </div>
    </div>
</div>
@endsection
