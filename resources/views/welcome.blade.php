@extends('layouts.main')

@section('content')
<div class="container">
	<div class="row justify-content-center">
		<div class="col-md-8">
			<div class="card">
				<div class="card-header">Generador de QR</div>

				<div class="card-body">
					<form action="{{ route('qr') }}" enctype='multipart/form-data' method="POST">
						@csrf
						<div class="form-group row">
							<label for="Foto" class="col-md-4 col-form-label text-md-right">{{ __('Foto') }}</label>
							<div class="input-group col-md-6">
								<div class="custom-file @error('Foto') is-invalid @enderror">
									<input type="file" class=" @error('Foto') is-invalid @enderror" name="Foto" id="validatedInputGroupCustomFile" >
									{{-- <label class="custom-file-label" for="validatedInputGroupCustomFile">Choose file...</label> --}}
								</div>
								@error('Foto')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
							<div class="col-md-2">
								<button type="button" class="btn btn-link" data-toggle="popover" title="Recomendaciones" data-content="Dimensiones: 189x228 px <br>
								Peso: 58 KB">?</button>
							</div>
						</div>
						<div class="form-group row">
							<label for="NOMBRE" class="col-md-4 col-form-label text-md-right">{{ __('NOMBRE') }}</label>

							<div class="col-md-6">
								<input id="NOMBRE" type="text" class="form-control @error('NOMBRE') is-invalid @enderror" name="NOMBRE" value="{{ old('NOMBRE') }}"  autocomplete="NOMBRE" autofocus>

								@error('NOMBRE')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="APELLIDOS" class="col-md-4 col-form-label text-md-right">{{ __('APELLIDOS') }}</label>

							<div class="col-md-6">
								<input id="APELLIDOS" type="text" class="form-control @error('APELLIDOS') is-invalid @enderror" name="APELLIDOS" value="{{ old('APELLIDOS') }}"  autocomplete="APELLIDOS" autofocus>

								@error('APELLIDOS')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="CURP" class="col-md-4 col-form-label text-md-right">{{ __('CURP') }}</label>

							<div class="col-md-6">
								<input id="CURP" type="text" class="form-control @error('CURP') is-invalid @enderror" name="CURP" value="{{ old('CURP') }}"  autocomplete="CURP" autofocus>

								@error('CURP')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="NACIONALIDAD" class="col-md-4 col-form-label text-md-right">{{ __('NACIONALIDAD') }}</label>

							<div class="col-md-6">
								<input id="NACIONALIDAD" type="text" class="form-control @error('NACIONALIDAD') is-invalid @enderror" name="NACIONALIDAD" value="{{ old('NACIONALIDAD') }}"  autocomplete="NACIONALIDAD" autofocus>

								@error('NACIONALIDAD')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="Fecha Nacimiento" class="col-md-4 col-form-label text-md-right">{{ __('FECHA NACIMIENTO') }}</label>

							<div class="col-md-6">
								<input id="Fecha Nacimiento" type="text" class="form-control @error('Fecha Nacimiento') is-invalid @enderror" name="FECHA_NACIMIENTO" value="{{ old('Fecha Nacimiento') }}"  autocomplete="Fecha Nacimiento" autofocus>

								@error('Fecha Nacimiento')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="SEXO" class="col-md-4 col-form-label text-md-right">{{ __('SEXO') }}</label>

							<div class="col-md-6">
								<input id="SEXO" type="text" class="form-control @error('SEXO') is-invalid @enderror" name="SEXO" value="{{ old('SEXO') }}"  autocomplete="SEXO" autofocus>

								@error('SEXO')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="DOCUMENTO" class="col-md-4 col-form-label text-md-right">{{ __('DOCUMENTO') }}</label>

							<div class="col-md-6">
								<input id="DOCUMENTO" type="text" class="form-control @error('DOCUMENTO') is-invalid @enderror" name="DOCUMENTO" value="{{ old('DOCUMENTO') }}"  autocomplete="DOCUMENTO" autofocus>

								@error('DOCUMENTO')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="NUE" class="col-md-4 col-form-label text-md-right">{{ __('NUE') }}</label>

							<div class="col-md-6">
								<input id="NUE" type="text" class="form-control @error('NUE') is-invalid @enderror" name="NUE" value="{{ old('NUE') }}"  autocomplete="NUE" autofocus>

								@error('NUE')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="FECHA_EXPEDICION" class="col-md-4 col-form-label text-md-right">{{ __('FECHA_EXPEDICION') }}</label>

							<div class="col-md-6">
								<input id="FECHA_EXPEDICION" type="text" class="form-control @error('FECHA_EXPEDICION') is-invalid @enderror" name="FECHA_EXPEDICION" value="{{ old('FECHA_EXPEDICION') }}"  autocomplete="FECHA_EXPEDICION" autofocus>

								@error('FECHA_EXPEDICION')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<div class="form-group row">
							<label for="OR_EXPEDICION" class="col-md-4 col-form-label text-md-right">{{ __('OR_EXPEDICION') }}</label>

							<div class="col-md-6">
								<input id="OR_EXPEDICION" type="text" class="form-control @error('OR_EXPEDICION') is-invalid @enderror" name="OR_EXPEDICION" value="{{ old('OR_EXPEDICION') }}"  autocomplete="OR_EXPEDICION" autofocus>

								@error('OR_EXPEDICION')
									<span class="invalid-feedback" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							</div>
						</div>
						<button type="submit" class="btn btn-primary">Submit</button>
					</form>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection
@section('js')
	<script>
		$('[data-toggle="popover"]').popover({
			template:'<div class="popover" role="tooltip"><div class="arrow"></div><h3 class="popover-header"></h3><div class="popover-body"></div></div>',
			html: true,
		})
	</script>
@endsection